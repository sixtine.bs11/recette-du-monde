import React from 'react';

class SearchRecipe extends React.Component {
    constructor (props){
        super(props);
        this.state = {
            text: this.props.category,
        }
    }

    handleCategory = (e) => {
        this.setState({
            text: e.target.value
        })
    };
    // Function for recover the curent value
    handleSubmit = (e) => {
        e.preventDefault();
        this.props.handleCategory(this.state.text);
    };

    render() {
        return (
            <>
                <h1 className={"title"}>Travel through your plate</h1>
                <form onSubmit={this.handleSubmit} >
                    <input type={"text"} name={"searchRecipe"} onChange={this.handleCategory}/>
                    <button type="submit">OK</button>
                </form>
            </>
        )
    }
}

export default SearchRecipe;