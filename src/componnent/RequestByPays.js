import React from 'react';
import Axios from "axios";

class RequestByPays extends React.Component{
    constructor(props) {
        super(props);
        this.state = {
            area: []
        }
    }

    // request by pays
    componentDidMount() {
        const recoverRecipeByPays = this.props.pays;
        Axios.get('https://www.themealdb.com/api/json/v1/1/filter.php?', {
            params: {
                a: recoverRecipeByPays
            },
        })
            .then((response)=> {
                console.log(response);
                const areaRes = response.data.meals;
                this.setState({pays: areaRes})
            })
            .catch(function (error) {
                console.log(error)
            });
    }

    componentDidUpdate(prevProps) {
    if (this.props.area !== prevProps.area) {
        this.area(this.props.areaRes)
    }
    }

    render() {
        return(
            <>
                {this.state.areaRes.map(areaMeal => {areaMeal.strMeal})}
            </>
        )
    }

}

export default RequestByPays;