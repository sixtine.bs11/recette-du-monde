import React from 'react';
import Canadian from "../img/ca.png";
import Chinese from "../img/cn.png";
import Egyptian from "../img/eg.png";
import Spanish from "../img/es.png";
import French from "../img/fr.png";
import British from "../img/gb.png";
import Greek from "../img/gr.png";
import Indian from "../img/in.png";
import Irish from "../img/ir.png"
import Italian from "../img/it.png";
import Jamaican from "../img/jm.png";
import Japanese from "../img/jp.png";
import Kenyan from "../img/kn.png";
import Moroccan from "../img/ma.png";
import Mexican from "../img/mx.png";
import Malaysian from "../img/my.png";
import Dutch from "../img/nl.png";
import Russian from "../img/ru.png";
import Thai from "../img/th.png";
import Tunisian from "../img/tn.png";
import Turkish from "../img/tr.png";
import American from "../img/us.png";
import Vietnamese from "../img/vn.png";

class Flags extends React.Component{
        constructor(props) {
                super(props);
                this.state = {
                        name: this.props.pays,
                };
        }

        handleSubmitPays = (e) => {
                this.setState({
                        name: e.target.value
                });

        };

        handleSubmit = (e) => {
                e.preventDefault();
                this.props.handlePays(this.state.name)
        };

    render(){
        return(
            <>
                <img src={Canadian}  alt={"Canadian"} name={"Canadian"} onClick={this.handleSubmitPays}/>
                <img src={Chinese}  alt={"Chinese"} name={"Chinese"} onClick={this.handleSubmitPays}/>
                <img src={Egyptian}  alt={"Egyptian"} name={"Egyptian"} onClick={this.handleSubmitPays}/>
                <img src={Spanish}  alt={"Spanish"} name={"Spanish"} onClick={this.handleSubmitPays}/>
                <img src={French}  alt={"French"} name={"French"} onClick={this.handleSubmitPays}/>
                <img src={British}  alt={"British"} name={"British"} onClick={this.handleSubmitPays}/>
                <img src={Greek}  alt={"Greek"}  name={"Greek"} onClick={this.handleSubmitPays}/>
                <img src={Indian}  alt={"Indian"}  name={"Indian"} onClick={this.handleSubmitPays}/>
                <img src={Irish}  alt={"Irish"}  name={"Irish"} onClick={this.handleSubmitPays}/>
                <img src={Italian}  alt={"Italian"}  name={"Italian"} onClick={this.handleSubmitPays}/>
                <img src={Jamaican}  alt={"Jamaican"}  name={"Jamaican"} onClick={this.handleSubmitPays}/>
                <img src={Japanese}  alt={"Japanese"}  name={"Japanese"} onClick={this.handleSubmitPays}/>
                <img src={Kenyan}  alt={"Kenyan"}  name={"Kenyan"} onClick={this.handleSubmitPays}/>
                <img src={Moroccan}  alt={"Moroccan"} name={"Moroccan"} onClick={this.handleSubmitPays}/>
                <img src={Mexican}  alt={"Mexican"} name={"Mexican"} onClick={this.handleSubmitPays}/>
                <img src={Malaysian}  alt={"Malaysian"} name={"Malaysian"} onClick={this.handleSubmitPays}/>
                <img src={Dutch}  alt={"Dutch"} name={"Dutch"} onClick={this.handleSubmitPays}/>
                <img src={Russian}  alt={"Russian"} name={"Russian"} onClick={this.handleSubmitPays}/>
                <img src={Thai}  alt={"Thai"} name={"Thai"} onClick={this.handleSubmitPays}/>
                <img src={Tunisian}  alt={"Tunisian"} name={"Tunisian"} onClick={this.handleSubmitPays}/>
                <img src={Turkish}  alt={"Turkish"} name={"Turkish"} onClick={this.handleSubmitPays}/>
                <img src={American}  alt={"American"} name={"American"} onClick={this.handleSubmitPays}/>
                <img src={Vietnamese}  alt={"Vietnamese"} name={"Vietnamese"} onClick={this.handleSubmitPays}/>
            </>
        )
    }
}

export default Flags;