import React from 'react';
import Axios from "axios";

class Request extends React.Component{
    constructor(props) {
        super(props);
        this.state = {
            meals: []

        };
    }

    RequestByName = () => {
        const recoverRecipeByName = this.props.category;
        Axios.get('https://www.themealdb.com/api/json/v1/1/search.php?', {
            params: {
                s: recoverRecipeByName
            }
        })
            .then((response) => {

                const mealsRes = response.data.meals;
                console.log(mealsRes);
                this.setState({ meals : mealsRes });

            })
            .catch(function (error) {
                console.log(error)
            });
    };
        // request by name
    componentDidMount() {
        this.RequestByName();
    }

    componentDidUpdate(prevProps) {
        if(this.props.category !== prevProps.category) {
            console.log(`update ${this.state.category}`);
            this.RequestByName();
        }
    }

    render() {
        return (

            <ul>
                {this.state.meals.map(Meal => (
                    <li>{Meal.strMeal}</li>
                ))}
            </ul>

        );

    }
 }


export default Request;