import React from 'react';
import './App.css';
import './css/searchRecipe.css'
import SearchRecipe from "./componnent/searchRecipe";
import Flags from "./componnent/flags";
import EndPage from "./componnent/EndPage";
import Request from "./componnent/Request";

class App extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            category: 'cake',
            pays:''
        }
    }


    handleValueCategory = (text) => {
        this.setState(state => ({
            category: text
        }))
    };

    handleValuePays = (name) => {
        this.setState(state => ({
            pays: name
        }))
    };

    render() {

        return (
            <>
                <header>
                    <div className={"conteneurSearchRecipe"}>
                        <SearchRecipe handleCategory={this.handleValueCategory}/>

                    </div>
                </header>
                <section>
                    <Flags handlePays={this.handleValuePays}/>
                    <Request category={this.state.category}/>
                </section>
                <footer>
                    <EndPage/>
                </footer>
            </>
        );
    }

}

export default App;
